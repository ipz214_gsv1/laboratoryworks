﻿#include <stdio.h>
#include <windows.h>
#include <time.h>

int checkbit(const int value, const int position) {
    int result;
    if ((value & (1 << position)) == 0) {
        result = 0;
    }
    else {
        result = 1;
    }
    return result;
}
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    union floatf {
        float number;
        struct {
            int mantissa : 23;
            unsigned char harakter : 8;
            unsigned char znak : 1;
        };
    };
    floatf qq = {};
    float input;
    size_t len = sizeof(float) * CHAR_BIT;
    size_t i;
    printf("Введіть число:"); scanf_s("%f", &input);
    qq.number = input;
    printf("\nЗнак: %d", checkbit(qq.number, len-1));
    printf("\nЗначення побітово:");
    for (int i = 0; i < len;i++) {
        printf(" %d ", checkbit(qq.number, i));
    }
    printf("\nМантиса:");
    for (int i = 22; i >=0;i--) {
        printf( "%d ",checkbit(qq.number,i));
    }
    printf("\nХарактеристика:");
    for (int i = 30; i >=23 ; i--) {
        printf(" %d ", checkbit(qq.number, i));
    }
    printf("\n sizeof(floatf) = %d", sizeof(floatf));
}
