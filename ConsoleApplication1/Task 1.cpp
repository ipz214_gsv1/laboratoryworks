﻿#include <stdio.h>
#include <windows.h>
#include <time.h>
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	struct date{
		unsigned char nSeconds : 6;
		unsigned char nMinutes : 6;
		unsigned char nHours : 5;
		unsigned char nWeekDay : 3;
		unsigned char nMonthDay : 6;
		unsigned char nMonth : 5;
		unsigned char nYear : 8;
   };
	date date{ 10, 11, 12, 4,15,7,22 };
	tm time;
	time.tm_year = 22;
	time.tm_mon = 7;
	time.tm_mday = 15;
	time.tm_wday = 4;
	time.tm_hour = 12;
	time.tm_min = 11;
	time.tm_sec = 10;
	printf("Структура date займає: %d\n", sizeof(date));
	printf("Стандартна структура tm займає: %d\n",sizeof(time));
	printf("Вивід даних на екран:\n");
	printf("Год: %d, Месяц: %d, День месяца: %d, День недели: %d, Часы: %d, Минуты : %d, Секунды: %d", date.nYear, date.nMonth, date.nMonthDay, date.nWeekDay, date.nHours, date.nMinutes, date.nSeconds);
}
