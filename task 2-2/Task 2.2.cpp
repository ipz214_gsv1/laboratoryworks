﻿#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <limits.h>

int checkbit(const int value, const int position) {
    int result;
    if ((value & (1 << position)) == 0) {
        result = 0;
    }
    else {
        result = 1;
    }
    return result;
}
void main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed short a = 0;
    size_t len = sizeof(signed short) * CHAR_BIT;
    size_t i;
    printf("Введіть число:");
    scanf_s("%d",&a);
    if (checkbit(a, len - 1) == 0) {
        printf("Останній біт числа дорівнює: %d, тому знак +", checkbit(a, len - 1));
    }
    else printf("Останній біт числа дорівнює: %d, тому знак -", checkbit(a, len - 1));
}
